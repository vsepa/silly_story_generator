var customName = document.getElementById('customname');
var randomize = document.querySelector('.randomize');
var story = document.querySelector('.story');

function randomValueFromArray(array) {
  return array[Math.floor(Math.random() * array.length)];
}

var storyText = "Было 94 фарингейта снаружи, поэтому :insertx: пошел на прогулку. Когда они пришли к :inserty:, они были в ужасе какоето время, а затем :insertz:. Иван увидел все это, но не был удивлен — :insertx: весил 300 фунтов, и это был жаркий день.";
var insertX = ["Уменьшатель","Молот Гнева","Ледяная Скорбь"];
var insertY = ["Гравитационная Пушка","Диснейлэнд","Леденящая пыль"];
var insertZ = ["Вилли Гоблин","Большой Папа","Черная Дыра Аскабана"];

randomize.addEventListener('click', result);

function result() {
  var newStory = storyText;

  var xItem = randomValueFromArray(insertX);
  var yItem = randomValueFromArray(insertY);
  var zItem = randomValueFromArray(insertZ);

  newStory = newStory.replace(":insertx:", xItem).replace(":insertx:", xItem).replace(":inserty:", yItem).replace(":insertz:", zItem);

  if (customName.value !== '') {
    var name = customName.value;
    newStory = newStory.replace("Иван", name);
  }

  if (document.getElementById("ru").checked) {
    var weight = Math.round(300*2.204) + " килограммы";
    var temperature = Math.round((94 - 32) * 5 / 9) + " градуса";
    newStory = newStory.replace("94 фарингейта", temperature).replace("300 фунтов", weight);
  }

  story.textContent = newStory;
  story.style.visibility = 'visible';
}
